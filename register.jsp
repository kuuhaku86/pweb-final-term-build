<%-- 
    Document   : register
    Created on : Nov 25, 2020, 6:52:14 PM
    Author     : Yohan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Register</title>
    <link rel="stylesheet" href="./lib/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="./css/login-register.css" />
	<script src="./lib/jquery/dist/jquery.js"> </script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="/login">Login</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="/register">Register<span class="sr-only">(current)</span></a>
                </li>
                </ul>
            </div>
            </nav>
		<div class="right_col" role="main">
    <div class="back-image bg bg1">
        <h1 class="header-w3ls">
        </h1>
        <div class="main ">
            <div class="headder-icon">
            <div class="top-icon ">
                <span class="fa fa-angle-double-down" aria-hidden="true"></span>
            </div>
            </div>
            <div class="its-sign-in">
			<c:if test="${error != null}">
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					<strong>${error}</strong>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			</c:if>
            <h2 class="">Register</h2>
            </div>
				<form method="POST" action="/register">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-12" style="margin-bottom:10px">
								<input type="email" name="email" class="form-control" id="email" />
                            </div>
                            <div class="col-12" style="margin-bottom:10px">
								<input type="text" name="namaLengkap" class="form-control" id="namaLengkap" />
                            </div>
                            <div class="col-12">
								<input type="password" name="password" class="form-control" id="password" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="btnn">
                            <button type="submit" value="Save" class="btn">Register</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="copy" style="padding-bottom:20px">
            </div>
        </div>
    </div>
    </body>
	<script>
		$(function() {
			$("#email").attr("placeholder", "Email");
			$("#password").attr("placeholder", "Password");
			$("#namaLengkap").attr("placeholder", "Nama Lengkap");
			$("#email").attr("required", true);
			$("#password").attr("required", true);
			$("#namaLengkap").attr("required", true);
			$(".close").click(function(){
				$(".alert").hide();
			});
		});
	</script>
</html>