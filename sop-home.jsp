<%-- 
    Document   : sop-home
    Created on : Nov 27, 2020, 9:57:04 PM
    Author     : Yohan
--%>

<%@page import="java.util.Iterator"%>
<%@page import="com.myapp.struts.model.SOP"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>SOP</title>
        <link rel="stylesheet" href="./lib/bootstrap/dist/css/bootstrap.min.css" />
        <link rel="stylesheet" href="./css/homepage.css" />
            <script src="./lib/jquery/dist/jquery.js"> </script>
        <link href="//fonts.googleapis.com/css?family=Oswald:300,400,500,600&display=swap" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,900&display=swap" rel="stylesheet">
		<style>
			#Content {
				color: black;
				margin: 0 50px;
			}

			h1 {
				text-decoration-style: solid;
				text-decoration-color: black;
			}
		</style> 
    </head>
    <body>
        <%@ include file = "header-home.jspf" %>
		<div id="Content">
			<h1 style="text-align:center;">SOP</h1>
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Aturan</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                        ArrayList<SOP> sops = SOP.getAll();
                        int count = 1;
                        for(Iterator i = sops.iterator(); i.hasNext();){
                            SOP curSOP = (SOP)i.next();
                            out.println("<tr>");
                            out.println("<td>" + count + "</td>");
                            out.println("<td>" + curSOP.getSop_value()+ "</td>");
                            out.println("</tr>");
                            count++;
                        }
                    %>
                </tbody>
            </table>
		</div>
        <%@ include file = "footer-home.jspf" %>
    </body>
</html>


