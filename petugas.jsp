<%-- 
    Document   : petugas
    Created on : Nov 27, 2020, 7:28:09 PM
    Author     : Zahra
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.LinkedList"%>
<%@page import="com.myapp.struts.model.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./lib/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="./css/dashboard.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<title>Petugas</title>
</head>
<body class="nav-md admin">

    <%@ include file = "layout.jspf" %>
    
    <div class="right_col" role="main">
    <div class="text-center">
        <h1 class="display-4">Petugas</h1>
            <% 
                Object errorParam = request.getAttribute("error");
                if(errorParam != null){
                    boolean error = (Boolean)errorParam;
                    if(error){
                        out.println("<div class=\"alert alert-danger\" role=\"alert\">");
                        out.println("Failed to delete Petugas");
                        out.println("</div>");
                    }
                }
            %>
            <div class="row mb-2">
                <div class="col-10"></div>
                <div class="col-2">
                    <a href="./petugas/add">
                        <button type="button" class="btn btn-primary">Add</button>
                    </a>
                </div>
            </div>
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Lengkap</th>
                        <th>Email</th>
                        <th style="width: 15%;">Action</th>
                    </tr>
                </thead>
                <tbody>
                <%
                    LinkedList<User> petugas = (LinkedList<User>)request.getAttribute("petugas");
                    int count = 1;
                    for(Iterator i = petugas.iterator(); i.hasNext();){
                        User curPetugas = (User)i.next();
                        out.println("<tr>");
                        out.println("<td>" + count + "</td>");
                        out.println("<td>" + curPetugas.getNamaLengkap()+ "</td>");
                        out.println("<td>" + curPetugas.getEmail()+ "</td>");
                        out.println("<td>");
                        out.println("<form action=\"/dashboard/petugas/edit\" method=\"get\" style=\"display:inline-block\" >");
                        out.println("<button name=\"id\" value=\""+ curPetugas.getUserId() +"\" class=\"btn btn-warning\"><i class=\"fa fa-edit\"></i></button>");
                        out.println("</form>");
                        out.println("<form action=\"/dashboard/petugas/delete\" method=\"post\" style=\"display:inline-block\" >" );
                        out.println("<button name=\"id\" value=\""+ curPetugas.getUserId() +"\" class=\"btn btn-danger\" onclick=\"return confirm('Are you sure you want to delete this?')\"><i class=\"fa fa-trash\"></i></button>");
                        out.println("</form>");
                        out.println("</td>");
                        out.println("</tr>");
                        count++;
                    }
                %>
                </tbody>
            </table>
        </div>
    </div>
    
    <script src="../lib/jquery/dist/jquery.min.js"></script>
    <script src="../lib/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../js/site.js"></script>
    <script src="../js/dashboard.js"></script>
</body>
</html>