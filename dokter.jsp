<%-- 
    Document   : dokter
    Created on : Nov 28, 2020, 2:07:54 PM
    Author     : asus
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.LinkedList"%>
<%@page import="com.myapp.struts.model.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="/lib/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/css/dashboard.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Dokter</title>
</head>
<body class="nav-md admin">

    <%@ include file = "layout.jspf" %>
    
    <div class="right_col" role="main">
    <div class="text-center">
        <h1 class="display-4">Dokter</h1>
            <div class="row mb-2">
                <div class="col-10"></div>
                <div class="col-2">
                    <a href="/dashboard/dokter/add">
                        <button type="button" class="btn btn-primary">Add</button>
                    </a>
                </div>
            </div>
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Lengkap</th>
                        <th>Email</th>
                        <th style="width: 15%;">Action</th>
                    </tr>
                </thead>
                <tbody>
                <%
                    LinkedList<User> dokter = (LinkedList<User>)request.getAttribute("dokter");
                    int count = 1;
                    for(Iterator i = dokter.iterator(); i.hasNext();){
                        User curDokter = (User)i.next();
                        out.println("<tr>");
                        out.println("<td>" + count + "</td>");
                        out.println("<td>" + curDokter.getNamaLengkap()+ "</td>");
                        out.println("<td>" + curDokter.getEmail()+ "</td>");
                        out.println("<td>");
                        out.println("<form action=\"/dashboard/dokter/edit\" method=\"get\" style=\"display:inline-block\" >");
                        out.println("<button name=\"id\" value=\""+ curDokter.getUserId() +"\" class=\"btn btn-warning\"><i class=\"fa fa-edit\"></i></button>");
                        out.println("</form>");
                        out.println("<form action=\"/dashboard/dokter/delete\" method=\"post\" style=\"display:inline-block\" >" );
                        out.println("<button name=\"id\" value=\""+ curDokter.getUserId() +"\" class=\"btn btn-danger\" onclick=\"return confirm('Are you sure you want to delete this?')\"><i class=\"fa fa-trash\"></i></button>");
                        out.println("</form>");
                        out.println("</td>");
                        out.println("</tr>");
                        count++;
                    }
                %>
                </tbody>
            </table>
        </div>
    </div>
    
    <script src="/lib/jquery/dist/jquery.min.js"></script>
    <script src="/lib/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="/js/site.js"></script>
    <script src="/js/dashboard.js"></script>
</body>
</html>