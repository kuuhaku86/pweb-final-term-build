<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../lib/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../css/dashboard.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Insert Pasien</title>
</head>
<body class="nav-md admin">

    <%@ include file = "layout.jspf" %>
    
    <div class="right_col" role="main">
        <div class="text-center">
            <h1 class="display-4">Insert Pasien</h1>
            <% 
                Object errorParam = request.getAttribute("error");
                if(errorParam != null){
                    boolean error = (Boolean)errorParam;
                    if(error){
                        out.println("<div class=\"alert alert-danger\" role=\"alert\">");
                        out.println("Gagal menambahkan pasien");
                        out.println("</div>");
                    }else{
                        out.println("<div class=\"alert alert-success\" role=\"alert\">");
                        out.println("Sukses menambahkan pasien");
                        out.println("</div>");
                    }
                }
            %>
            <form method="POST">
                <div asp-validation-summary="ModelOnly" class="text-danger"></div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-2">
                            <label for="Name" class="control-label">Name</label>
                        </div>
                        <div class="col-10">
                            <input name="Name" id="Name" type="text" class="form-control" required />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-2">
                            <label for="Address" class="control-label">Address</label>
                        </div>
                        <div class="col-10">
                            <input name="Address" id="Adress" type="text" class="form-control" required />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-2">
                            <label for="InfectionDate" class="control-label">Infection Date</label>
                        </div>
                        <div class="col-10">
                            <input name="InfectionDate" id="InfectionDate" type="date" class="form-control" required/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-2">
                            <label for="ContactNumber" class="control-label">Contact Number</label>
                        </div>
                        <div class="col-10">
                            <input name="ContactNumber" id="ContactNumber" type="text" class="form-control" required />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" value="Add" class="btn btn-primary" />
                </div>
            </form>
        </div>
    </div>


    <script src="../lib/jquery/dist/jquery.min.js"></script>
    <script src="../lib/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../js/site.js"></script>
    <script src="../js/dashboard.js"></script>
</body>
</html>