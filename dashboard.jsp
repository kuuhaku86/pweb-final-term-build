<%-- 
    Document   : sop
    Created on : Nov 27, 2020, 1:30:46 PM
    Author     : Yohan
--%>

<%@page import="java.util.Iterator"%>
<%@page import="com.myapp.struts.model.SOP"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<c:set var="title" value="Dashboard"/>
<%@ include file = "layout.jspf" %>
    <div class="right_col" role="main">
        <div class="text-center">
			<h1>Dashboard For Authenticated User</h1>
        </div>
    </div>

    <script src="/lib/jquery/dist/jquery.min.js"></script>
    <script src="/lib/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="/js/site.js"></script>
    <script src="/js/dashboard.js"></script>
</body>
</html>
