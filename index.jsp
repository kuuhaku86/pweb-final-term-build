<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Homepage</title>
        <link rel="stylesheet" href="./lib/bootstrap/dist/css/bootstrap.min.css" />
        <link rel="stylesheet" href="./css/homepage.css" />
            <script src="./lib/jquery/dist/jquery.js"> </script>
        <link href="//fonts.googleapis.com/css?family=Oswald:300,400,500,600&display=swap" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,900&display=swap" rel="stylesheet">
		<style>
			body {
			  background-image: url('https://cdn0-production-images-kly.akamaized.net/vxH1Sdq8HVlYKgFInP7jQEf51t8=/1231x710/smart/filters:quality(75):strip_icc():format(webp)/kly-media-production/medias/3069252/original/031155300_1583405571-20200305-Medis-Corona-Korea-Selatan-1.jpg');
			  background-repeat: no-repeat;
			  background-attachment: fixed;
			  background-size: cover;
			  text-align: center;
			  vertical-align: middle;
			}
			body:after {
				content: '\A';
				position: absolute;
				width: 100%; height:100%;
				top:0; left:0;
				background:rgba(0,0,0,1);
				opacity: 0.35;
				transition: all 1s;
				-webkit-transition: all 1s;
			}

			#Content {
				position: absolute;
				top: 50%;
				left: 50%;
				color: white;
				transform: translate(-50%, -50%);
				-ms-transform: translate(-50%, -50%); /* IE 9 */
				-webkit-transform: translate(-50%, -50%); /* Chrome, Safari, Opera */	
				z-index: 9999;
			}

			h1, h3 {
				text-decoration-style: solid;
				text-decoration-color: black;
			}
			.top-header-content, .footer-hny {
				position: absolute;
				z-index: 9999;
			}
		</style> 
    </head>
    <body>
        <%@ include file = "header-home.jspf" %>
		<div id="Content">
			<h1>Pantau COVID-19</h1>
			<h3>Web yang didedikasikan untuk memantau penyebaran COVID-19</h3>
			<h3>Bagi pihak medis dan relawan yang ingin membantu pihak medis</h3>
		</div>
        <%@ include file = "footer-home.jspf" %>
    </body>
</html>

