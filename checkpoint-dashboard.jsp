
<%@page import="java.util.Iterator"%>
<%@page import="com.myapp.struts.model.Checkpoint"%>
<%@page import="java.util.LinkedList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<c:set var="title" value="Checkpoint"/>
<%@ include file = "layout.jspf" %>
    <div class="right_col" role="main">
        <div class="text-center">
            <h1 class="display-4">Checkpoint</h1>
			<% 
				Object errorParam = request.getAttribute("error");
				Object successParam = request.getAttribute("success");
				if(errorParam != null){
					out.println("<div class=\"alert alert-danger\" role=\"alert\">");
					out.println(errorParam);
					out.println("</div>");
				} else if (successParam != null) {
					out.println("<div class=\"alert alert-success\" role=\"alert\">");
					out.println(successParam);
					out.println("</div>");
				}
			%>
            <div class="row mb-2">
                <div class="col-10"></div>
                <div class="col-2">
                    <a href="/dashboard/checkpoint/add">
                        <button type="button" class="btn btn-primary">Add</button>
                    </a>
                </div>
            </div>
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th style="width: 15%;">Id</th>
                        <th>Place</th>
                        <th style="width: 15%;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                    
                        LinkedList<Checkpoint> checkpoints = Checkpoint.getAll();
                        int count = 1;
                        for(Iterator i = checkpoints.iterator(); i.hasNext();){
                            Checkpoint curCp = (Checkpoint)i.next();
                            out.println("<tr>");
                            out.println("<td>" + count + "</td>");
                            out.println("<td>" + curCp.getValue()+ "</td>");
                            out.println("<td>");
                            out.println("<form action=\"/dashboard/checkpoint/edit\" method=\"get\" style=\"display:inline-block\" >");
                            out.println("<button name=\"id\" value=\""+ curCp.getId() +"\" class=\"btn btn-warning\"><i class=\"fa fa-edit\"></i></button>");
                            out.println("</form>");
                            out.println("<form action=\"/dashboard/checkpoint/delete\" method=\"post\" style=\"display:inline-block\" >" );
                            out.println("<button name=\"id\" value=\""+ curCp.getId() +"\" class=\"btn btn-danger\" onclick=\"return confirm('Are you sure you want to delete this?')\"><i class=\"fa fa-trash\"></i></button>");
                            out.println("</form>");
                            out.println("</td>");
                            out.println("</tr>");
                            count++;
                        }
                    %>
                </tbody>
            </table>
        </div>
    </div>

    <script src="/lib/jquery/dist/jquery.min.js"></script>
    <script src="/lib/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="/js/site.js"></script>
    <script src="/js/dashboard.js"></script>
</body>
</html>
