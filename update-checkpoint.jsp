
<%@page import="com.myapp.struts.model.Checkpoint"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.myapp.struts.model.SOP"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<c:set var="title" value="Update Checkpoint"/>
<%@ include file = "layout.jspf" %>
    <div class="right_col" role="main">
        <div class="text-center">
            <h1 class="display-4">Insert Checkpoint</h1>
            <% 
                Checkpoint checkpoint = (Checkpoint)request.getAttribute("checkpoint");
                Object errorParam = request.getAttribute("error");
                if(errorParam != null){
                    out.println("<div class=\"alert alert-danger\" role=\"alert\">");
                    out.println("Failed to insert checkpoint");
                    out.println("</div>");
                }
            %>
            <form method="POST">
                <div class="text-danger"></div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-2">
                            <label for="value" class="control-label">Place</label>
                        </div>
                        <div class="col-10">
                            <textarea name="value" class="form-control" rows="3" placeholder="Place" required ><% out.write(checkpoint.getValue()); %></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" value="Add" class="btn btn-primary" />
                </div>
            </form>
        </div>
    </div>


    <script src="../lib/jquery/dist/jquery.min.js"></script>
    <script src="../lib/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../js/site.js"></script>
    <script src="../js/dashboard.js"></script>
</body>
</html>
