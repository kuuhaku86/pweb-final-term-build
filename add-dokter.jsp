<%-- 
    Document   : add-dokter
    Created on : Nov 28, 2020, 2:21:42 PM
    Author     : asus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="/lib/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/css/dashboard.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Insert Dokter</title>
</head>
<body class="nav-md admin">

    <%@ include file = "layout.jspf" %>
    
    <div class="right_col" role="main">
    <div class="text-center">
            <h1 class="display-4">Insert Dokter</h1>
            <% 
                Object errorParam = request.getAttribute("error");
                if(errorParam != null){
                    boolean error = (Boolean)errorParam;
                    if(error){
                        out.println("<div class=\"alert alert-danger\" role=\"alert\">");
                        out.println("Gagal menambahkan dokter");
                        out.println("</div>");
                    }else{
                        out.println("<div class=\"alert alert-success\" role=\"alert\">");
                        out.println("Sukses menambahkan dokter");
                        out.println("</div>");
                    }
                }
            %>
            <form method="POST">
                <div class="form-group">
                    <div class="row">
                        <div class="col-2">
                            <label for="email" class="control-label">Email</label>
                        </div>
                        <div class="col-10">
                            <input name="email" id="email" type="email" class="form-control" required />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-2">
                            <label for="namaLengkap" class="control-label">Nama Lengkap</label>
                        </div>
                        <div class="col-10">
                            <input name="namaLengkap" id="namaLengkap" type="text" class="form-control" required />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-2">
                            <label for="password" class="control-label">Password</label>
                        </div>
                        <div class="col-10">
                            <input name="password" id="password" type="password" class="form-control" required/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" value="Add" class="btn btn-primary" />
                </div>
            </form>
        </div>
    </div>


    <script src="/lib/jquery/dist/jquery.min.js"></script>
    <script src="/lib/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="/js/site.js"></script>
    <script src="/js/dashboard.js"></script>
</body>
</html>
