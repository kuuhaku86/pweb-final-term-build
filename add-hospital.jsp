<%-- 
    Document   : add-hospital
    Created on : Nov 26, 2020, 8:39:35 PM
    Author     : Zahra
--%>

<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Insert Hospital</title>
        <link rel="stylesheet" href="../lib/bootstrap/dist/css/bootstrap.min.css" />
        <link rel="stylesheet" href="../css/homepage.css" />
            <script src="../lib/jquery/dist/jquery.js"> </script>
        <link href="//fonts.googleapis.com/css?family=Oswald:300,400,500,600&display=swap" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <%@ include file = "header-home.jspf" %>
        
        <div class="text-center">
            <h1 class="display-4" style="margin: 20px 0; font-size: 40px;">Insert Hospital</h1>
            <% 
                Object errorParam = request.getAttribute("error");
                if(errorParam != null){
                    boolean error = (Boolean)errorParam;
                    if(error){
                        out.println("<div class=\"alert alert-danger\" role=\"alert\">");
                        out.println("Failed to add Hospital");
                        out.println("</div>");
                    }else{
                        out.println("<div class=\"alert alert-success\" role=\"alert\">");
                        out.println("Success to add Hospital");
                        out.println("</div>");
                    }
                }
            %>
            <form method="POST">
                <center>
                <div class="form-group">
                    <div class="row">
                        <div class="col-2">
                            <label for="name" class="control-label">Name</label>
                        </div>
                        <div class="col-9">
                            <input name="name" id="name" type="text" class="form-control" required />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-2">
                            <label for="address" class="control-label">Address</label>
                        </div>
                        <div class="col-9">
                            <input name="address" id="address" type="text" class="form-control" required />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" value="Add" class="btn btn-primary" />
                </div>
                </center>
            </form>
        </div>
            
        <%@ include file = "footer-home.jspf" %>

        <script src="../js/jquery.magnific-popup.js"></script>
        <script src="../js/homepage.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    </body>
</html>


