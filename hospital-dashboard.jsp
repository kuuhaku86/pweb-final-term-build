<%-- 
    Document   : hospital-dashboard
    Created on : Nov 28, 2020, 1:18:38 AM
    Author     : Yohan
--%>

<%@page import="com.myapp.struts.model.Hospital"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.LinkedList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<c:set var="title" value="Hospital"/>
<%@ include file = "layout.jspf" %>
    <div class="right_col" role="main">
            <div class="text-center">
                <h1 class="display-4">Hospital</h1>
                <% 
                    Object errorParam = request.getAttribute("error");
                    if(errorParam != null){
                        boolean error = (Boolean)errorParam;
                        if(error){
                            out.println("<div class=\"alert alert-danger\" role=\"alert\">");
                            out.println("Failed to delete Hospital");
                            out.println("</div>");
                        }
                    }
                %>
                <div class="row mb-2">
                    <div class="col-10"></div>
                    <div class="col-2">
                        <a href="./hospital/add">
                            <button type="button" class="btn btn-primary">Add</button>
                        </a>
                    </div>
                </div>
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th style="width: 15%;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <%
                        LinkedList<Hospital> hospital = (LinkedList<Hospital>)request.getAttribute("hospital");
                        int count = 1;
                        for(int i = 0; i < hospital.size(); i++){
                            Hospital curHospital = hospital.get(i);
                            out.println("<tr>");
                            out.println("<td>" + count + "</td>");
                            out.println("<td>" + curHospital.getName()+ "</td>");
                            out.println("<td>" + curHospital.getAddress()+ "</td>");
                            out.println("<td>");
                            out.println("<form action=\"/hospital/edit\" method=\"get\" style=\"display:inline-block\" >");
                            out.println("<button name=\"id\" value=\""+ curHospital.getHospitalId() +"\" class=\"btn btn-warning\"><i class=\"fa fa-edit\"></i></button>");
                            out.println("</form>");
                            out.println("<form action=\"/hospital/delete\" method=\"post\" style=\"display:inline-block\" >" );
                            out.println("<button name=\"id\" value=\""+ curHospital.getHospitalId() +"\" class=\"btn btn-danger\" onclick=\"return confirm('Are you sure you want to delete this?')\"><i class=\"fa fa-trash\"></i></button>");
                            out.println("</form>");
                            out.println("</td>");
                            out.println("</tr>");
                            count++;
                        }
                    %>
                    </tbody>
                </table>
            </div>
		</div>
        <script src="./js/jquery.magnific-popup.js"></script>
        <script src="./js/homepage.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>

