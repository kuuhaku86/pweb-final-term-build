<%-- 
    Document   : form-sop
    Created on : Nov 27, 2020, 1:31:09 PM
    Author     : Yohan
--%>


<%@page import="java.util.Iterator"%>
<%@page import="com.myapp.struts.model.SOP"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<c:choose>

    <c:when test="${isUpdate.equals('true')}">
		<c:set var="title" value="Update SOP"/>
    </c:when>    
    <c:otherwise>
		<c:set var="title" value="Insert SOP"/>
    </c:otherwise>
</c:choose>
<%@ include file = "layout.jspf" %>
    <div class="right_col" role="main">
        <div class="text-center">
			<c:choose>
				<c:when test="${isUpdate.equals('true')}">
					<h1 class="display-4">Update SOP</h1>
				</c:when>    
				<c:otherwise>
					<h1 class="display-4">Insert SOP</h1>
				</c:otherwise>
			</c:choose>
            <% 
                Object errorParam = request.getAttribute("error");
                if(errorParam != null){
					out.println("<div class=\"alert alert-danger\" role=\"alert\">");
					out.println(request.getAttribute("error").toString());
					out.println("</div>");
                }
            %>
			<c:choose>
				<c:when test="${isUpdate.equals('true')}">
				<form method="POST" action="/dashboard/sop/update">
				</c:when>    
				<c:otherwise>
				<form method="POST" action="/dashboard/sop/add">
				</c:otherwise>
			</c:choose>
                <div asp-validation-summary="ModelOnly" class="text-danger"></div>
                <div class="form-group">
                    <div class="row">
						<c:if test="${isUpdate.equals('true')}">
							<input type="hidden" name="id" value="${id}" />
						</c:if>
                        <div class="col-2">
                            <label for="value" class="control-label">Value</label>
                        </div>
                        <div class="col-10">
                            <textarea name="value" class="form-control" rows="3" placeholder="Value" required >
								<c:if test="${isUpdate.equals('true')}">
									${value.trim()}
								</c:if>
							</textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
					<c:choose>
						<c:when test="${isUpdate.equals('true')}">
						<input type="submit" value="Update" class="btn btn-primary" />
						</c:when>    
						<c:otherwise>
						<input type="submit" value="Add" class="btn btn-primary" />
						</c:otherwise>
					</c:choose>
                </div>
            </form>
        </div>
    </div>


    <script src="../lib/jquery/dist/jquery.min.js"></script>
    <script src="../lib/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../js/site.js"></script>
    <script src="../js/dashboard.js"></script>
</body>
</html>
