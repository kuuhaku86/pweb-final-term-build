<%@page import="java.time.LocalDate"%>
<%@page import="com.myapp.struts.model.Pasien"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../lib/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../css/dashboard.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Update Pasien</title>
</head>
<body class="nav-md admin">

    <%@ include file = "layout.jspf" %>
    <%
        Object errorParam = request.getAttribute("error");
        String name = (String)request.getAttribute("name");
        String address = (String)request.getAttribute("address");
        String date = (String)request.getAttribute("infectionDate");
        String contactNumber = (String)request.getAttribute("contactNumber");
    %>
    
    <div class="right_col" role="main">
        <div class="text-center">
            <h1 class="display-4">Update Pasien</h1>
            <% 
                if(errorParam != null){
                    boolean error = (Boolean)errorParam;
                    if(error){
                        out.println("<div class=\"alert alert-danger\" role=\"alert\">");
                        out.println("Invalid Param");
                        out.println("</div>");
                    }
                }
            %>
            <form method="POST">
                <div asp-validation-summary="ModelOnly" class="text-danger"></div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-2">
                            <label for="Name" class="control-label">Name</label>
                        </div>
                        <div class="col-10">
                            <input name="Name" id="Name" type="text" class="form-control" value="<% out.print(name); %>" required />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-2">
                            <label for="Address" class="control-label">Address</label>
                        </div>
                        <div class="col-10">
                            <input name="Address" id="Adress" type="text" class="form-control" value="<% out.print(address); %>"  required />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-2">
                            <label for="InfectionDate" class="control-label">Infection Date</label>
                        </div>
                        <div class="col-10">
                            <input name="InfectionDate" id="InfectionDate" type="date" class="form-control" value="<% out.print(date); %>"  required/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-2">
                            <label for="ContactNumber" class="control-label">Contact Number</label>
                        </div>
                        <div class="col-10">
                            <input name="ContactNumber" id="ContactNumber" type="text" class="form-control" value="<% out.print(contactNumber); %>"  required />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" value="Update" class="btn btn-primary" />
                </div>
            </form>
        </div>
    </div>


    <script src="../lib/jquery/dist/jquery.min.js"></script>
    <script src="../lib/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../js/site.js"></script>
    <script src="../js/dashboard.js"></script>
</body>
</html>