<%-- 
    Document   : update-dokter
    Created on : Nov 28, 2020, 2:27:16 PM
    Author     : asus
--%>


<%@page import="java.sql.ResultSet"%>
<%@page import="com.myapp.struts.utils.DBHandler"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="/lib/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/css/dashboard.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Dokter</title>
</head>
<body class="nav-md admin">

    <%@ include file = "layout.jspf" %>
    
    <div class="right_col" role="main">
    <div class="text-center">
            <h1 class="display-4">Update Dokter</h1>
            <% 
                Object errorParam = request.getAttribute("error");
                if(errorParam != null){
                    boolean error = (Boolean)errorParam;
                    if(error){
                        out.println("<div class=\"alert alert-danger\" role=\"alert\">");
                        out.println("Gagal mengubah data dokter");
                        out.println("</div>");
                    }else{
                        out.println("<div class=\"alert alert-success\" role=\"alert\">");
                        out.println("Sukses mengubah data dokter");
                        out.println("</div>");
                    }
                }
            %>
            <form method="POST">
                <% 
                    try
                    {
                        int id = Integer.parseInt(request.getParameter("id"));
                        DBHandler db = new DBHandler();
                        ResultSet queryResult = db.query("SELECT * FROM users where user_id="+id);

                        while(queryResult.next())
                        {
                        %>
                <div class="form-group">
                    <div class="row">
                        <div class="col-2">
                            <label for="email" class="control-label">Email</label>
                        </div>
                        <div class="col-10">
                            <input value="<%=queryResult.getString("email") %>" name="email" id="email" type="email" class="form-control" required />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-2">
                            <label for="namaLengkap" class="control-label">Nama Lengkap</label>
                        </div>
                        <div class="col-10">
                            <input value="<%=queryResult.getString("nama_lengkap") %>" name="namaLengkap" id="namaLengkap" type="text" class="form-control" required />
                        </div>
                    </div>
                </div>
                <%
                        }
                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                        out.println("Error " +ex.getMessage());
                    }

                %>
                <div class="form-group">
                    <input type="submit" value="Update" class="btn btn-primary" />
                </div>
            </form>
        </div>
    </div>


    <script src="/lib/jquery/dist/jquery.min.js"></script>
    <script src="/lib/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="/js/site.js"></script>
    <script src="/js/dashboard.js"></script>
</body>
</html>

