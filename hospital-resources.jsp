<%-- 
    Document   : hospital-resources
    Created on : Nov 27, 2020, 1:18:18 PM
    Author     : Zahra
--%>

<%@page import="com.myapp.struts.model.HospitalResources"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.LinkedList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Hospital Resources</title>
        <link rel="stylesheet" href="./lib/bootstrap/dist/css/bootstrap.min.css" />
        <link rel="stylesheet" href="./css/homepage.css" />
            <script src="./lib/jquery/dist/jquery.js"> </script>
        <link href="//fonts.googleapis.com/css?family=Oswald:300,400,500,600&display=swap" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <%@ include file = "header-home.jspf" %>
            <div class="text-center">
                <h1 class="display-4">Hospital Resources</h1>
                <% 
                    Object errorParam = request.getAttribute("error");
                    if(errorParam != null){
                        boolean error = (Boolean)errorParam;
                        if(error){
                            out.println("<div class=\"alert alert-danger\" role=\"alert\">");
                            out.println("Failed to delete Hospital Resource");
                            out.println("</div>");
                        }
                    }
                %>
                <div class="row mb-2">
                    <div class="col-10"></div>
                    <div class="col-2">
                    </div>
                </div>
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Hospital</th>
                            <th>Resource</th>
                            <th>Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                    <%
                        LinkedList<HospitalResources> hospital_resources = (LinkedList<HospitalResources>)request.getAttribute("hospital_resources");
                        int count = 1;
                        for(Iterator i = hospital_resources.iterator(); i.hasNext();){
                            HospitalResources curHospitalResources = (HospitalResources)i.next();
                            out.println("<tr>");
                            out.println("<td>" + count + "</td>");
                            out.println("<td>" + curHospitalResources.getHospitalName()+ "</td>");
                            out.println("<td>" + curHospitalResources.getName()+ "</td>");
                            out.println("<td>" + curHospitalResources.getQuantity()+ "</td>");
                            out.println("</tr>");
                            count++;
                        }
                    %>
                    </tbody>
                </table>
            </div>
        <%@ include file = "footer-home.jspf" %>
        
        <script src="./js/jquery.magnific-popup.js"></script>
        <script src="./js/homepage.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    </body>
</html>