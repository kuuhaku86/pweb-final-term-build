<%-- 
    Document   : add-hospital-resources
    Created on : Nov 27, 2020, 1:18:43 PM
    Author     : Zahra
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="com.myapp.struts.utils.DBHandler"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Hospital Resources - Insert</title>
        <link rel="stylesheet" href="../lib/bootstrap/dist/css/bootstrap.min.css" />
        <link rel="stylesheet" href="../css/homepage.css" />
            <script src="../lib/jquery/dist/jquery.js"> </script>
        <link href="//fonts.googleapis.com/css?family=Oswald:300,400,500,600&display=swap" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <%@ include file = "header-home.jspf" %>
        
        <div class="text-center">
            <h1 class="display-4" style="margin: 20px 0; font-size: 40px;">Insert Hospital Resources</h1>
            <% 
                Object errorParam = request.getAttribute("error");
                if(errorParam != null){
                    boolean error = (Boolean)errorParam;
                    if(error){
                        out.println("<div class=\"alert alert-danger\" role=\"alert\">");
                        out.println("Failed to add Hospital Resource");
                        out.println("</div>");
                    }else{
                        out.println("<div class=\"alert alert-success\" role=\"alert\">");
                        out.println("Success to add Hospital Resource");
                        out.println("</div>");
                    }
                }
            %>
            <form method="POST">
                <div class="form-group">
                    <div class="row">
                        <div class="col-2">
                            <label for="hospitalId" class="control-label">Hospital</label>
                        </div>
                        <div class="col-9">
                            <select name="hospitalId" class="form-control" required >
                                <option value="0">Choose Hospital</option>
                                <% 
                                    try
                                    {
                                        DBHandler db = new DBHandler();
                                        ResultSet queryResult = db.query("SELECT hospital_id, hospital_name FROM hospitals;");
                                        
                                        while(queryResult.next())
                                        {
                                        %>
                                            <option value="<%=queryResult.getInt(1)%>"><%=queryResult.getString(2)%></option>
                                        <%
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        ex.printStackTrace();
                                        out.println("Error " +ex.getMessage());
                                    }

                                %>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-2">
                            <label for="name" class="control-label">Name</label>
                        </div>
                        <div class="col-9">
                            <input name="name" id="name" type="text" class="form-control" required />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-2">
                            <label for="quantity" class="control-label">Quantity</label>
                        </div>
                        <div class="col-9">
                            <input name="quantity" id="quantity" type="integer" class="form-control" required />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" value="Add" class="btn btn-primary" />
                </div>
            </form>
        </div>
            
        <%@ include file = "footer-home.jspf" %>

        <script src="../js/jquery.magnific-popup.js"></script>
        <script src="../js/homepage.js"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    </body>
</html>
