<%-- 
    Document   : homepage
    Created on : Nov 26, 2020, 4:44:56 PM
    Author     : Zahra
--%>

<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Homepage</title>
        <link rel="stylesheet" href="./lib/bootstrap/dist/css/bootstrap.min.css" />
        <link rel="stylesheet" href="./css/homepage.css" />
            <script src="./lib/jquery/dist/jquery.js"> </script>
        <link href="//fonts.googleapis.com/css?family=Oswald:300,400,500,600&display=swap" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,900&display=swap" rel="stylesheet">
    </head>
    <body>
        <%@ include file = "header-home.jspf" %>
			
        <%@ include file = "footer-home.jspf" %>
    </body>
</html>
